wallcomp-UI
===========

Dependencies
------------

Once cloned, get dependencies using:
```
$ npm install
```
- *boostrap*: provides some base css files to make things look nice
- *font-awesome*: used for tab icons
- *jquery*: requirement for bootstrap and jquery-ui
- *jquery-ui*: used for sliders and other "fancy" widgets
- *expressjs*: server managing
- *pg*: SQL client, requires *postgresql* for server (available on most linux package managers) 

Run
---

Start the server:
```
$ sudo node server.js
```
To get the ip-address of the device, run (for linux):
```
$ ifconfig
```
