// DATABASE (I have no idea what I'm doing...)
var fs = require('fs');

var express = require('express');
var path = require('path');

var app = express();

// [ Change relative paths of node_modules to be in /public.
//   Provides a nice distinction between our code and imported libraries. ]

app.use('/', express.static(__dirname + '/views'));
app.use('/public', express.static(__dirname + '/public'));

app.use('/public/scripts', express.static(__dirname + '/node_modules/jquery/dist'));

app.use('/public/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/public/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/public/fonts', express.static(__dirname + '/node_modules/bootstrap/fonts'));
app.use('/public/css', express.static(__dirname + '/node_modules/bootstrap'));

app.use('/public/css', express.static(__dirname + '/node_modules/font-awesome/css'));
app.use('/public/fonts', express.static(__dirname + '/node_modules/font-awesome/fonts'));

app.use('/public/scripts', express.static(__dirname + '/node_modules/jquery-ui/ui'));
app.use('/public/css', express.static(__dirname + '/node_modules/jquery-ui/themes/base'));

app.use('/public/scripts', express.static(__dirname + '/node_modules/ace-builds/src-noconflict'));

var Pool = require('pg').Pool;

var config = {
  host: 'localhost',
  user: 'user',             // PGUSER
  database: 'wallcompdb',   // PGDATABASE
  password: 'wallcompbris', // PGPASSWORD
  port: 5432,               // PGPORT
  max: 10,
  idleTimeoutMillis: 30000,
};

var pool = new Pool(config);

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/views/code.html'));
});

app.get('/api', function(req,res) {
  switch (req.query.command) {
    case "query": {
      pool.connect(function(err, client, done) {
        if(err) return console.error('error fetching client from pool', err);

        client.query('SELECT id, name, tag1, tag2, date FROM program', function(err, result) {
          done(); //call `done()` to release the client back to the pool
          if(err) return console.error('error running query', err);

          console.log(result.rows);
          res.json(result.rows);
        });
      });
      break;
    }
    case "submit": {
      pool.connect(function(err, client, done) {
        if (err) return console.error('error fetching client from pool', err);

        client.query(
          'INSERT INTO program (name, tag1, tag2, date) VALUES ($1, $2, $3, $4) RETURNING id', 
          [req.query.data.name, req.query.data.tag1, req.query.data.tag2, new Date()], 
          function(err, result) {
            done(); //call `done()` to release the client back to the pool
            if(err) return console.error('error running query', err);
            
            // code savefile is based off programs unique database id
            // TODO: compile and check for errors before submission
            fs.writeFile(__dirname + "/code/" + result.rows[0].id + ".x", req.query.data.code, function(err) {
              if (err) return console.error(err);
              console.log(result.rows[0].id + ".x FILE SAVED!");
            });
          }
        );
      });

      res.send(req.query.command);
      break;
    }
    case "examine": {
      fs.readFile(__dirname + "/code/" + req.query.data.id + ".x", function(err, data) {
        if (err) return console.error(err);
        res.send(data.toString());
      });
    }
  }
});

pool
  .query(
    "CREATE TABLE IF NOT EXISTS program (" +
    "  id   SERIAL NOT NULL PRIMARY KEY,"  +
    "  name varchar(32) NOT NULL,"         +
    "  tag1 varchar(16),"                  +
    "  tag2 varchar(16),"                  +
    "  date timestamp"                     +
    ")")
  .then(function() {
    app.listen(80, function () {
      console.log('App listening on port 80.');
    })
  });
