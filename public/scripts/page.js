var help = 1;

function toggleHelp() {
  if(help == 1) {
    $("#help").animate({width:0}, 1000);
    $("#help").fadeOut();
    $("#script").animate({width:'83%'}, 1000);
  } else {
    $("#help").animate({width:'25%'}, 1000);
    $("#help").fadeIn();
    $("#script").animate({width:'50%'}, 1000);
  }
}

function getWords(sel) {
  var raw = $(sel).html();
  var p0 = raw.split(">");
  var words = [];
  var newlinesAtEnd = 0;

  for(var x = 0; x < p0.length; x++) {
    var p1 = p0[x].split("<");

    if(p1.length == 2) {
      var wrds = p1[0].trim().split(" ");
      for(var w = 0; w < wrds.length; w++) {
        if(wrds[w] != "") {
          words.push(wrds[w]);
          newlinesAtEnd = 0;
        }
      }

      if(p1[1].substr(0,2) == "br") {
        words.push("///brnl");
        newlinesAtEnd++;
      }
    } else if(p1[0].substr(0,2) == "br") {
      words.push("///brnl");
      newlinesAtEnd++;
    } else if(x + 1 == p0.length) {
      var wrds = p1[0].trim().split(" ");
      for(var w = 0; w < wrds.length; w++) {
        if(wrds[w] != "") {
          words.push(wrds[w]);
          newlinesAtEnd = 0;
        }
      }
    }
  }

  for(var i = 0; i < newlinesAtEnd; i++) {
    words[words.length - i - 1] = "";
  }

  return words;
}

function lintWord(word) {
  var startIndex = word.indexOf("\"");

  if(startIndex == -1) {
    return word;
  } else {
    var startwrd = word.substr(0, startIndex);
    var remwrd = word.substr(startIndex + 1, word.length - startIndex);
    var nextBracket = remwrd.indexOf("\"") + 1;

    if(nextBracket == 0) {
      return startwrd + "<span style='color:#F00;'>\"" + remwrd + "</span>";
    } else {
      return startwrd + "<span style='color:#F00;'>\"" + remwrd.substr(0,nextBracket) + "</span>" + lintWord(remwrd.substr(nextBracket, remwrd.length - nextBracket));
    }

  }
}

var isComment = 0;
function lintCode() {
  var words = getWords("code");
  $("code").empty();
  $(".lineNumbers").empty();
  var lintedCode = "";
  var lineNos = 0;

  for(var i = 0; i < words.length; i++) {
    if(words[i] == "func" && isComment == 0) {
      lintedCode = lintedCode + "<span style='color:#22B;'>" + words[i] + "</span>";
    } else if((words[i] == "is" || words[i] == "return") && isComment == 0) {
      lintedCode = lintedCode + "<span style='color:#22B; font-weight:bold;'>" + words[i] + "</span>";
    } else if(words[i] == ";" || words[i].substr(0,1) == ";") {
      lintedCode = lintedCode + "<span style='color:#0D0; font-weight:bold;'>" + words[i];
      isComment = 1;
    } else if(words[i] == "///brnl") {
      lineNos = lineNos + 1;

      if(isComment == 1) {
        isComment = 0;
        lintedCode = lintedCode + "</span>";
      }
      lintedCode = lintedCode + "<br/>";
    } else {
      lintedCode = lintedCode + lintWord(words[i]);
    }

    lintedCode = lintedCode + " ";

    if(i + 1 == words.length && words[i] != "///brnl") {
      lineNos = lineNos + 1;
    }
  }

  $("code").append(lintedCode);

  $(".lineNumbers").append("1");
  for(var i = 1; i < lineNos; i++) {
    $(".lineNumbers").append("<br>" + (i + 1));
  }
  curLine = lineNos;
}

var curLine = 2;
var curWord = "";
$("#code").focusout(function(event) {
    lintCode();
});
$("#code").on("keypress", "code", function(event) {
  if(event.keyCode == 13) {
    curLine = curLine + 1;
    $(".lineNumbers").append("<br>" + curLine);
  }
});
//lintCode();
